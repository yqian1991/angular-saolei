from random import randint
import json

class Cell:
    def __init__(self):
        self._isMine = False
        self._isRevealed = False
        self._isFlagged = False
        self._minesAround = 0

    def isMine(self):
        return self._isMine

    def setMine(self):
        self._isMine = True

    def isRevealed(self):
        return self._isRevealed

    def isFlagged(self):
        return self._isFlagged

    def getMinesAround(self):
        return self._minesAround

    def setMineNumberAround(self, minesAround):
        self._minesAround = minesAround

    def setFlag(self):
        self._isFlagged = True

    def unsetFlag(self):
        self._isFlagged = False

    def setRevealed(self):
        self._isRevealed = True

    def unsetRevealed(self):
        self._isRevealed = False

class Board:
    def __init__(self, rows, cols, minesNumber):
        self._rows = rows
        self._cols = cols
        self._minesNumber = minesNumber
        self._board = []
        self._mines = []
        self._result = 0 # lose:-1,  ongoing:0, win:1

    def getResult(self):
        return self._result

    def mines(self):
        return self._mines

    def getBoard(self):
        return self._board

    #Init the board, with normal
    def setBoard(self):
        for row in range(self._rows):
            rowCells = []
            for col in range(self._cols):
                rowCells.append(Cell())
            self._board.append(rowCells)

        self.initMinesPosition()
        self.initCell()

    #generate mines positions
    def initMinesPosition(self):
        while len(self._mines) < self._minesNumber:
            row = randint(0, self._rows - 1)
            col = randint(0, self._cols - 1)
            if (row,col) not in self._mines:
                self._board[row][col].setMine()
                self._mines.append((row,col))

    #Init cell set minesAround
    def initCell(self):
        for row in range(self._rows):
            for col in range(self._cols):
                number = 0
                for (x,y) in self.getNeighbors(row,col):
                    if self._board[x][y].isMine():
                        number += 1
                self._board[row][col].setMineNumberAround(number)

    #Get neighbor cells
    def getNeighbors(self, row, col):
        neighborCells = []
        eightNeighbors = [(row-1, col-1), (row-1, col), (row-1, col+1),
                          (row, col-1), (row, col+1),
                          (row+1, col-1), (row+1, col), (row+1, col+1),
                          ]
        for (x, y) in eightNeighbors:
            if 0 <= x < self._rows and 0 <= y < self._cols:
                neighborCells.append((x, y))
        return neighborCells

    #Triggered by left click
    def updateBoard_reveal(self, x, y):
        cell = self._board[x][y]
        if not cell.isRevealed() and not cell.isFlagged():
            if cell.isMine():
                self._result = -1
            elif cell.getMinesAround() == 0:#means we need to update more cells
                self.updateNeighborsRecursively(x, y)
            cell.setRevealed()
            self.updateGameStatus()

    def updateNeighborsRecursively(self, row, col):
        cell = self._board[row][col]
        if not cell.isRevealed():
            cell.setRevealed()
            if cell.getMinesAround() == 0:
                for (r, c) in self.getNeighbors(row, col):
                    ncell = self._board[r][c]
                    if not ncell.isRevealed() and not ncell.isFlagged():
                        self.updateNeighborsRecursively(r, c)

    #Triggered by right click
    def updateBoard_flag(self, x, y):
        if self._board[x][y].isFlagged():
            self._board[x][y].unsetFlag()
        else:
            self._board[x][y].setFlag()
        self.updateGameStatus()

    def updateGameStatus(self):
        emptyCells = 0
        for rows in self._board:
            for cell in rows:
                if cell.isRevealed() == False or cell.isFlagged():
                    emptyCells += 1
        if emptyCells == self._minesNumber:
            self._result = 1

class JSONBoard:
    @classmethod
    def toJson(cls, board, result):
        boardView=[]
        for rows in board:
            rowsView = []
            for cell in rows:
                if result is not 0 and cell.isMine():
                    rowsView.append(["X", True, cell.isFlagged()])
                else:
                    if cell.isRevealed():
                        if cell.getMinesAround()>0:
                            rowsView.append([cell.getMinesAround(),True, cell.isFlagged()])
                        else:
                            rowsView.append(["", True, cell.isFlagged()])
                    else:
                        rowsView.append(["", False, cell.isFlagged()])
            boardView.append(rowsView)
        board_json = json.loads(json.dumps(boardView, default=lambda o: o.__dict__))
        return board_json

    @classmethod
    def toJson_showmines(cls, board):
        boardView = []
        for rows in board:
            rowsView = []
            for cell in rows:
                if cell.isMine():
                    rowsView.append("X")
                else:
                    rowsView.append("")
            boardView.append(rowsView)
        board_json = json.loads(json.dumps(boardView, default=lambda o: o.__dict__))
        return board_json

    @classmethod
    def toJson_showminesaround(cls, board):
        boardView = []
        for rows in board:
            rowsView = []
            for cell in rows:
                if cell.getMinesAround()>0:
                    rowsView.append(cell.getMinesAround())
                else:
                    rowsView.append("")
            boardView.append(rowsView)
        board_json = json.loads(json.dumps(boardView, default=lambda o: o.__dict__))
        return board_json