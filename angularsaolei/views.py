from pyramid.view import view_config
from models import Board, JSONBoard

@view_config(route_name='home', renderer='templates/index.pt')
def my_view(request):
    return {'project': 'angular-saolei'}

@view_config(route_name='api.init', renderer='json')
def start_game_view(request):
    print 'Init game...'
    global board
    board = Board(10, 10, 5)
    board.setBoard()
    print len(board.mines())
    return JSONBoard.toJson(board.getBoard(), board.getResult());

@view_config(route_name='api.update', renderer='json')
def update_view(request):
    print request.json_body
    row = int(request.json_body['row'])
    col = int(request.json_body['col'])
    board.updateBoard_reveal(row, col)
    return [JSONBoard.toJson(board.getBoard(), board.getResult()), board.getResult()];

@view_config(route_name='api.board', renderer='json')
def board_view(request):
    return [JSONBoard.toJson(board.getBoard(), board.getResult()), board.getResult()];

@view_config(route_name='api.mines', renderer='json')
def board_mines_view(request):
    return JSONBoard.toJson_showmines(board.getBoard());

@view_config(route_name='api.number', renderer='json')
def board_minesnumber_view(request):
    return JSONBoard.toJson_showminesaround(board.getBoard());

@view_config(route_name='api.state', renderer='json')
def state_view(request):
    return board.getResult()

@view_config(route_name='api.revealed', renderer='json')
def revealed_view(request):
    row = int(request.json_body['row'])
    col = int(request.json_body['col'])
    return board.getBoard()[row][col].isRevealed()

@view_config(route_name='api.flag', renderer='json')
def flag_view(request):
    row = int(request.json_body['row'])
    col = int(request.json_body['col'])
    board.updateBoard_flag(row, col)
    return [JSONBoard.toJson(board.getBoard(), board.getResult()), board.getResult()];


