import unittest

from pyramid import testing
from models import Cell, Board


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_my_view(self):
        from .views import my_view
        request = testing.DummyRequest()
        info = my_view(request)
        self.assertEqual(info['project'], 'angular-saolei')

    def test_model_create_board(self):
        board = Board(4, 4, 16)
        board.setBoard()
        for(x, y) in board.mines():
            self.assertEqual(board.getBoard()[x][y].isMine(), True)

