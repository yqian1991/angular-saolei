angular-saolei README

=====================
       Set up
=====================
1. mkvirtualenv angular

2. pip install pyramid

3. pcreate -s starter angular-tictactoe

Clean mytemplate.pt

4. Add bootstrap support
     <app>/static/css
     <app>/static/font
     <app>/static/js

5. Run python setup.py test -q to test
   Run python setup.py develop for development

6. pserve development.ini --reload

7. Add angular.js
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>

================
Deploy to Heroku
================
##Pre Deploy

### pip  freeze > requirements

### Add Procfile:  echo "web: ./run" > Procfile

### Add run, chmod +x run
  '''
  #!/bin/bash
set -e
python setup.py develop
python runapp.py
  '''

### Add runapp.py
'''
import os

from paste.deploy import loadapp
from waitress import serve

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app = loadapp('config:production.ini', relative_to='.')

    serve(app, host='0.0.0.0', port=port)
'''

##Deploy

### heroku create --stack cedar

### git push heroku master

### heroku scale web=1

### heroku open

### heroku ps

### heroku logs -t
